﻿var PrStockControllers = angular.module("PrStockControllers", []);
// this controller call the api method and display the list of employees  
// in list.html  
PrStockControllers.controller("ListController", ['$scope', '$http',
    function ($scope, $http) {
        $http.get("api/PrProduct/DetailedTypes").success(function (data) {
            $scope.Types = data;
        });
    }
]);
