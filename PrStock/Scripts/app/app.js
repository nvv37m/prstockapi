﻿var PrStockApp = angular.module('PrStockApp', ['ngRoute', 'PrStockControllers']);
PrStockApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/TypeList',
    {
        templateUrl: 'Prstock/TypeList.html',
        controller: 'ListController'
    }).
    when('/create',
    {
        templateUrl: '/Views/Prstock/edit.html',
        controller: 'EditController'
    }).
    when('/edit/:id',
    {
        templateUrl: '/Views/Prstock/edit.html',
        controller: 'EditController'
    }).
    when('/delete/:id',
    {
        templateUrl: 'Views/Prstock/delete.html',
        controller: 'DeleteController'
    }).
    otherwise(
    {
        redirectTo: '/TypeList'
    });
}]);