﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrStockModels.Models;
using PrStock.Models;
using PrStockModels.Repository;
using System.Threading.Tasks;

namespace PrStock.Controllers
{
    public class TransformPostView
    {
        private PrProductRepository PrDb = new PrProductRepository();
        private PrEventRepository EvDb = new PrEventRepository();
        private PrCompanyRepository CoDb = new PrCompanyRepository();
        private PrCategoryRepository CtDb = new PrCategoryRepository();
        private PrTypeRepository TyDb = new PrTypeRepository();
        private PrEmployeeRepository EmDb = new PrEmployeeRepository();


        public async Task AddProduct(PostProductDetailModel Product)
        {
            PrProductModels AddedProduct = PostProduct(Product);
            PrEventModels AddedEvent = PostEventForProduct(Product);
            await PrDb.InsertAsync(AddedProduct, AddedEvent, Product.Event.PrCategoryId, Product.Quantity, Product.Quantity );   
        }
        public void AddEvent(PostNewEventModel Event)
        {

            NewEventTransform(Event);
            
        }
        public void AddNewEvent(PostNewEventModel Event)
        {
            NewEventTransform(Event);
        }
        public async Task AddCompany(PostCompanyModel Company)
        {
            PrCompanyModels AddedCompany = PostCompany(Company);
            await CoDb.InsertAsync(AddedCompany);
        }
        public async Task AddCategory(CategoryViewModel Category)
        {
            PrCategoryModels AddedCategory = PostCategory(Category);
            await CtDb.InsertAsync(AddedCategory);
        }
        public async Task AddType(PostTypeModel Type)
        {
            PrTypeModels AddedType = PostType(Type);
            await TyDb.InsertAsync(AddedType);
        }
        private PrProductModels PostProduct(PostProductDetailModel Product)
        {
            
            PrProductModels PrProduct = new PrProductModels();
            PrProduct.ProductName = Product.ProductName;
            PrProduct.PrTypeModelsId = Product.TypeId;
            PrProduct.ProductTemplate = Product.TemplateUrl;
            PrProduct.ProductImage = Product.PictureUrl;
            PrProduct.ProductDetails = Product.Detail;
            PrProduct.ProductPrice = Product.Price;
            PrProduct.PrCompanyModelsId = Product.CompanyId;
            PrProduct.Stated = Product.ProductState;
      
            return PrProduct;
        }
        private PrEventModels PostNewEvent(PostNewEventModel _Event)
        {
            PrEventModels NewEvent = new PrEventModels
            {
                EventTitle = _Event.EventName,
                EventDescription = _Event.EventDescription,
                EventType = _Event.EventType,
                EventUrl = _Event.EventUri,
                EventEndDate = _Event.EndDate,
                EventStartDate = DateTime.Now,
                RespPersonModelsId = _Event.PersonId,
            };
            return NewEvent;

        }
        private PrEventModels PostEventForProduct(PostProductDetailModel Product)
        {

            PrEventModels ev = new PrEventModels()
            {
                EventTitle = Product.Event.EventName,
                EventDescription = Product.Event.EventDescription,
                EventStartDate = DateTime.Now,
                EventType = Product.Event.EventType,
                EventUrl = Product.Event.EventUri,
                RespPersonModelsId = Product.Event.PersonId,
            };
            return ev;
        }

        private PrEventModels PostEvent( PostEventModel AddEvent )
        {
            PrProductModels product = PrDb.GetProductbyId(AddEvent.ProductViewModelId);
            List<PrProductModels> products = new List <PrProductModels>();
            products.Add(product);
            PrEventModels addEvent = new PrEventModels()
            {
                EventTitle = AddEvent.EventName,
                EventDescription =AddEvent.EventDescription,
                EventType = AddEvent.EventType,
                EventUrl = AddEvent.EventUri,
                EventEndDate = AddEvent.EndDate,
                EventStartDate=DateTime.Now,
                RespPersonModelsId=AddEvent.PersonId,

            };
            return addEvent;
        }
        private PrCompanyModels PostCompany(PostCompanyModel AddCompany)
        {
            PrCompanyModels addCompany = new PrCompanyModels
            {
                CompanyName = AddCompany.PostCompanyName,
                CompanyContacts = AddCompany.CompanyContacts,
                CompanyWebsite = AddCompany.CompanyWebsite
            };
            return addCompany;
        }
        private PrCategoryModels PostCategory(CategoryViewModel AddCategory)
        {
            PrCategoryModels addCategory = new PrCategoryModels
            {
                Name = AddCategory.CategoryName
            };
            return addCategory;
        }
        private PrTypeModels PostType(PostTypeModel AddPost)
        {
            PrTypeModels addType = new PrTypeModels
            {
                Name = AddPost.TypeName
            };
            return addType;
        }
        private int FQuantity(int qIn, int qOut, string Func)
        {
           
            int fQuantity = new int();
            if (Func != "Invoice")
            {
                fQuantity = qIn-qOut;
            }
            else
            {
                fQuantity = qOut + qIn;
            }
            return fQuantity;
        }
        private void updateProduct(string Func, int Id, int Taken, PostEventModel Event )
        {
            PrProductModelsPrEventModels updatingProduct = EvDb.GetLastPrEventById(Id);
            PrEventModels _Event = PostEvent(Event);
            
            if (updatingProduct.ProdQuantity >= Taken)
            {
                int OutQuantity = FQuantity(updatingProduct.ProdQuantity, Taken, Func);

                PrDb.UpdateAsync(Id, OutQuantity, Taken, _Event);
            }
            else
            {
                if (Event.EventType == "Invoice")
                {
                    int OutQuantity= FQuantity(updatingProduct.Quantity, Taken, Func);
                    PrDb.UpdateAsync(Id, OutQuantity,Taken,_Event);
                }
            } 
                

        }
        public void NewEventTransform(PostNewEventModel _Event)
        {

            
            PrEventModels Event = PostNewEvent(_Event);
            List<PrProductModelsPrEventModels> PrEvs = new List<PrProductModelsPrEventModels>();
            foreach (EnumProduct pr in _Event.Products)
            {
                PrProductModelsPrEventModels updatingProduct = new PrProductModelsPrEventModels();
                updatingProduct = EvDb.GetLastPrEventById(pr.ProductId);
                PrProductModelsPrEventModels PrEv = new PrProductModelsPrEventModels
                {
                    PrProductModelsId = pr.ProductId,
                    Event = Event,
                    ProdQuantity = FQuantity(updatingProduct.ProdQuantity, pr.TakenQuantity, _Event.EventType),
                    Quantity = pr.TakenQuantity,
                    PrCategoryId = pr.CategoryId
                };
                
                PrEvs.Add(PrEv);
            }
            EvDb.AddNewEvent(PrEvs);
        }
       
    }
}