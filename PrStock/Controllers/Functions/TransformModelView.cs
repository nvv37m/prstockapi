﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrStock.Models;
using PrStockModels.Models;
using PrStockModels.Repository;
using System.Globalization;

namespace PrStock.Controllers
{
    public class TransformModelView
    {
        private PrTypeRepository dbT = new PrTypeRepository();
        private PrProductRepository dbP = new PrProductRepository();
        private PrCategoryRepository dbCa = new PrCategoryRepository();
        private PrCompanyRepository dbCo = new PrCompanyRepository();
        private PrEmployeeRepository dbEm = new PrEmployeeRepository();
        private PrEventRepository dbEv = new PrEventRepository();


        public List<DetailedTypeViewModel> GetAllTypes()
        {
            IEnumerable<PrTypeModels> prtypes = dbT.GetAllTypes();
            List<DetailedTypeViewModel> getalltypes = new List<DetailedTypeViewModel>();
            foreach (PrTypeModels pr in prtypes)
            {
                DetailedTypeViewModel type = new DetailedTypeViewModel();
                PrProductModels product = pr.Products.LastOrDefault();
                type.TypeName = pr.Name;
                type.Category = product.PrProductsPrEvents.LastOrDefault().Category.Name;
                type.DetailedTypeViewModelId = pr.PrTypeModelsId;
                type.Quantity = product.PrProductsPrEvents.LastOrDefault().Quantity;
                getalltypes.Add(type);
            }
            return getalltypes;
        }
        public List<TypeViewModel> GetTypesforEnum()
        {
            IEnumerable<PrTypeModels> prtypes = dbT.GetTypesForEnum();
            List<TypeViewModel> typesforenum = new List<TypeViewModel>();
            foreach (PrTypeModels pr in prtypes)
            {
                TypeViewModel type = new TypeViewModel();
                type.TypeViewModelId = pr.PrTypeModelsId;
                type.TypeName = pr.Name;
                typesforenum.Add(type);
            }
            return typesforenum;
        }
        public List<CompanyViewModel> GetCompaniesforEnum()
        {
            IEnumerable<PrCompanyModels> prcompanies = dbCo.GetAllCompanies();
            List<CompanyViewModel> companiesforenum = new List<CompanyViewModel>();
            foreach (PrCompanyModels co in prcompanies)
            {
                CompanyViewModel company = new CompanyViewModel();
                company.CompanyViewModelId = co.PrCompanyModelsId;
                company.CompanyName = co.CompanyName;
                company.CompanyWebsite = co.CompanyWebsite;
                companiesforenum.Add(company);
            }
            return companiesforenum;
        }
        
        public PrProductViewModel GetProductbyId(int id) {
            PrProductModels Product = new PrProductModels();
            PrProductViewModel _product = new PrProductViewModel();
            try
            {
                Product = dbP.GetProductbyId(id);
                PrProductModelsPrEventModels Staff = Product.PrProductsPrEvents.LastOrDefault();
                PrEventModels prevent = Staff.Event;
                _product.ProductViewModelId = Product.PrProductModelsId;
                _product.Name = Product.ProductName;
                _product.Type = Product.ProductType.Name;
                _product.PictureUrl = Product.ProductImage;
                _product.Person = prevent.RespPerson.RespFirstName + " " + prevent.RespPerson.RespLastName;
                _product.Category = Staff.Category.Name;
                _product.Quantity = Product.PrProductsPrEvents.LastOrDefault().ProdQuantity;
                _product.EventName = prevent.EventTitle;
                _product.EventTaken = Staff.Quantity;
                _product.StartDate = prevent.EventStartDate.ToString("d");
                if (_product.EndDate == null)
                {
                    _product.EndDate = prevent.EventEndDate.ToString();
                }
            }
            catch (Exception e)
            {
                _product.Name = "There is no such a product";
            }

            return _product;
        }
        public List<EmployeeViewModel> GetEmployeesForEnumiration()
        {
            List<EmployeeViewModel> employees = new List<EmployeeViewModel>();
            IEnumerable<RespPersonModels> Employees = dbEm.GetAllEmployees();
            foreach (RespPersonModels Employee in Employees)
            {
                EmployeeViewModel employee = new EmployeeViewModel
                {
                    EmployeeViewModelId = Employee.RespPersonModelsId,
                    FirstName = Employee.RespFirstName,
                    LastName = Employee.RespLastName,
                    ContactPhone = Employee.ContactPhone,
                    ContactEmail = Employee.ContactEmail
                };
                employees.Add(employee);
            }
            return employees;
        }
        public DetailedEventViewModel DetailedEvent(int id)
        {
            PrEventModels Events = dbEv.GetEventById(id);

            List<PrProductModelBriefView> products = new List<PrProductModelBriefView>();
            foreach (PrProductModelsPrEventModels pr in Events.PrProductsPrEvents)
            {
                PrProductModelBriefView product = new PrProductModelBriefView
                {
                   PrProductModelBriefId = pr.Product.PrProductModelsId,
                   Name = pr.Product.ProductName,
                   Quantity = pr.ProdQuantity,
                   State = pr.Product.Stated,
                   Type = pr.Product.ProductType.Name,
                   QuantityTaken=pr.Quantity,
                   
                };
                products.Add(product);
                
            };
            DetailedEventViewModel DetailedEvent = new DetailedEventViewModel
            {
                DetailedViewModelId = Events.PrEventModelsId,
                EventName = Events.EventTitle,
                EventDescription = Events.EventDescription,
                EventUrl = Events.EventUrl,
                EventType = Events.EventType,
                StartDate = Events.EventStartDate,
                EndDate = Events.EventEndDate,
                RespPerson = Events.RespPerson.RespFirstName + " " + Events.RespPerson.RespLastName,
                Products = products,
                
            };
            return DetailedEvent;
        }
        public List<PrProductModelBriefView> ProductBriefs()
        {
            IEnumerable<PrProductModels> products = dbP.GetAllProductsBrief();
            List<PrProductModelBriefView> productsBrief = new List<PrProductModelBriefView>();
            foreach (PrProductModels pr in products)
            {
                PrProductModelBriefView prBrief = new PrProductModelBriefView
                {
                    PrProductModelBriefId = pr.PrProductModelsId,
                    Name = pr.ProductName,
                    Quantity = pr.PrProductsPrEvents.LastOrDefault().ProdQuantity,
                    Type = pr.ProductType.Name
                    
                };
                productsBrief.Add(prBrief);
            }
            return productsBrief;
        }
        private List<PrProductModels> CheckForDuplicate(List<PrProductModels> InList)
        {
            List<PrProductModels> Outlist=InList.GroupBy(x=>x.PrProductModelsId).Select(grp=>grp.FirstOrDefault()).ToList();
            return Outlist;
        }
        public List<PrProductViewModel> GetAllProductsbyCat(string Cat)
        {
            IEnumerable<PrProductModels> Products = dbP.GetAllProductsByCategory(Cat);
            List<PrProductModels> SortedProduct = CheckForDuplicate(Products.ToList());
            List<PrProductViewModel> _products = new List<PrProductViewModel>();


            foreach (PrProductModels pr in SortedProduct)
            { 
                 
                PrProductViewModel _product = new PrProductViewModel();
                PrProductModelsPrEventModels Staff = pr.PrProductsPrEvents.LastOrDefault();
                PrEventModels prevent = Staff.Event;
                _product.ProductViewModelId = pr.PrProductModelsId;
                _product.Name = pr.ProductName;
                _product.Type = pr.ProductType.Name;
                _product.PictureUrl = pr.ProductImage;
                _product.Person = prevent.RespPerson.RespFirstName + " " + prevent.RespPerson.RespLastName;
                _product.Category = Staff.Category.Name;
                _product.State = pr.Stated;
                _product.Quantity = pr.PrProductsPrEvents.LastOrDefault().ProdQuantity;
                _product.EventName = prevent.EventTitle;
                _product.EventTaken = Staff.Quantity;
                _product.StartDate = prevent.EventStartDate.ToString("d");
                _product.EndDate = prevent.EventEndDate.ToString();
                _products.Add(_product);
            }
            return _products;
        }
        public List<PrProductViewModel> GetAllProducts(string Typo)
        {
            IEnumerable<PrProductModels> Products = dbP.GetAllProducts(Typo);
            List<PrProductViewModel> _products = new List<PrProductViewModel>();


            foreach (PrProductModels pr in Products)
            {
                PrProductViewModel _product = new PrProductViewModel();
                PrProductModelsPrEventModels Staff = pr.PrProductsPrEvents.LastOrDefault();
                PrEventModels prevent = Staff.Event;
                _product.ProductViewModelId = pr.PrProductModelsId;
                _product.Name = pr.ProductName;
                _product.Type = pr.ProductType.Name;
                _product.PictureUrl = pr.ProductImage;
                _product.Person = prevent.RespPerson.RespFirstName + " " + prevent.RespPerson.RespLastName;
                _product.Category = Staff.Category.Name;
                _product.State = pr.Stated;
                _product.Quantity = pr.PrProductsPrEvents.LastOrDefault().ProdQuantity;
                _product.EventName = prevent.EventTitle;
                _product.EventTaken = Staff.Quantity;
                _product.StartDate = prevent.EventStartDate.ToString("d");
                _product.EndDate = prevent.EventEndDate.ToString();
                _products.Add(_product);
            }
            return _products;
        }
        public ProductDetailViewModel GetProductDetailsById(int Id)
        {
            PrProductModels product = dbP.GetProductbyId(Id);
            if (product != null)
            {
                List<PrProductModelsPrEventModels> Staff = product.PrProductsPrEvents.ToList();
                ProductDetailViewModel _product = new ProductDetailViewModel();
                List<EventViewModel> _events = new List<EventViewModel>();
                _product.ProductName = product.ProductName;
                _product.ProductDetailViewModelId = product.PrProductModelsId;
                _product.Type = product.ProductType.Name;
                _product.Quantity = product.PrProductsPrEvents.LastOrDefault().ProdQuantity;
                _product.Price = product.ProductPrice;
                _product.State = product.Stated;
                _product.CompanyName = product.Company.CompanyName;
                _product.CompanyAddress = product.Company.CompanyWebsite;
                _product.Detail = product.ProductDetails;
                _product.CompanyContacts = new CompanyContactsViewModel
                {
                    FirstName = product.Company.CompanyContacts.FirstName,
                    LastName = product.Company.CompanyContacts.LastName,
                    Email = product.Company.CompanyContacts.Email,
                    Mobile = product.Company.CompanyContacts.Mobile
                };
                _product.PictureUrl = product.ProductImage;
                _product.TemplateUrl = product.ProductTemplate;

                {
                    foreach (PrProductModelsPrEventModels pr in Staff)
                    {
                        EventViewModel Event = new EventViewModel();
                        Event.EventViewModelId = pr.PrEventModelsId;
                        Event.Category = pr.Category.Name;
                        Event.EventName = pr.Event.EventTitle;
                        Event.EventType = pr.Event.EventType;
                        Event.Quantity = pr.Quantity;
                        Event.StartDate = pr.Event.EventStartDate;
                        Event.EndDate = pr.Event.EventEndDate;
                        Event.RespPerson = pr.Event.RespPerson.RespFirstName + " " + pr.Event.RespPerson.RespLastName;
                        _events.Add(Event);
                    }
                };
                _product.Events = _events;
                _product.PictureUrl = product.ProductImage;
                return _product;
            }
            else
            {
                ProductDetailViewModel _product = new ProductDetailViewModel();
                _product.ProductName = "There is no such a product";
                return _product;
            }

        }
        public List<PrProductModelBriefView> GetAllProducts()
        {
            List<PrProductModelBriefView> products = new List<PrProductModelBriefView>();
            List<PrProductModels> Products = dbP.GetAllProducts();
            foreach (PrProductModels Product in Products)
            {
                PrProductModelBriefView product = new PrProductModelBriefView();
                product.PrProductModelBriefId = Product.PrProductModelsId;
                product.Name = Product.ProductName;
                product.Quantity = Product.PrProductsPrEvents.LastOrDefault().ProdQuantity;
                product.QuantityTaken = Product.PrProductsPrEvents.LastOrDefault().Quantity;
                product.State = Product.Stated;
                product.Type = Product.ProductType.Name;
                product.Company = Product.Company.CompanyName;
                products.Add(product);
            }
            return products;
        }
        public List<CategoryViewModel> GetAllCategories()
        {
            List<CategoryViewModel> _categories = new List<CategoryViewModel>();
            List<PrCategoryModels> Categories = dbCa.GetCategoriesforEnum();
            foreach (PrCategoryModels Category in Categories)
            {
                CategoryViewModel _category = new CategoryViewModel();
                _category.CategoryViewModelId = Category.PrCategoryModelsId;
                _category.CategoryName = Category.Name;
                _categories.Add(_category);
            }
            return _categories;
        }
        public CategoryViewModel GetCategorybyId(int id)
        {
            CategoryViewModel _category = new CategoryViewModel();
            try
            {
                PrCategoryModels Category = dbCa.GetCategorybyId(id);
                _category.CategoryName = Category.Name;
                _category.CategoryViewModelId = Category.PrCategoryModelsId;
                return _category;
            }
            catch (Exception e)
            {
            _category.CategoryName = e.Message.ToString();
            return _category;
            }
        }
     } 
       
}
    
        
    
