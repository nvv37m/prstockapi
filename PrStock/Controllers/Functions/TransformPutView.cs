﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrStock.Models;
using PrStockModels.Models;
using PrStockModels.Repository;

namespace PrStock.Controllers
{
    public class TransformPutView
    {
        private PrProductRepository prDb = new PrProductRepository();
        private PrEventRepository evDb = new PrEventRepository();
        private TransformPostView tPv = new TransformPostView();
        public  void UpdateImage(PutProductImage UpdatedImage)
        {
            PrProductModels product = UpdatedProductImage(UpdatedImage);
            prDb.UpdateImage(product);
        }
        private PrProductModels UpdatedProductImage(PutProductImage UpdatedImage)
        {
            PrProductModels product = prDb.GetProductbyId(UpdatedImage.ProductId);
            product.ProductImage = UpdatedImage.ImageUrl;
            return product;
        }
        public void ReturnProduct(PostNewEventModel _Event)
        {
            CheckReturnedQuantity(_Event);
        }
        private void UpdateEvent(PostNewEventModel _Event, PrProductModelsPrEventModels prEv,EnumProduct pr)
        {
            prEv.PrCategoryId = pr.CategoryId;
            evDb.UpdateEvent(prEv);
        }
        public void UpdateEventEndDate(PostNewEventModel Event)
        {
            UpdateEventDate(Event);
        }
        private void UpdateEventDate(PostNewEventModel _Event)
        {
            PrEventModels Event = evDb.GetEventById(_Event.PostNewEventModelId);
            Event.EventEndDate = _Event.EndDate;
            evDb.UpdateEventDate(Event);
        }
        private void CheckReturnedQuantity( PostNewEventModel _Event)
        {
            int EvId = _Event.PostNewEventModelId;
            EnumProduct pr = _Event.Products[0];
            int PrId = pr.ProductId;
            PrProductModelsPrEventModels PrEv = evDb.GetPrEvbyId(PrId, EvId);
            if (PrEv.Quantity == pr.TakenQuantity)
            {
                UpdateEvent(_Event,PrEv,pr);
                tPv.AddNewEvent(_Event);
            }
            else if (PrEv.Quantity > pr.TakenQuantity)
            {
                tPv.AddNewEvent(_Event);
            }
        }
    }
}