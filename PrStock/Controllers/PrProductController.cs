﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PrStock.Models;
using PrStockModels.Models;
using PrStockModels.Repository;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using System.Web;
using System.Diagnostics;

namespace PrStock.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PrProductController : ApiController
    {

        private TransformModelView TrRepo = new TransformModelView();
        private TransformPostView TrPost = new TransformPostView();
        private TransformPutView TrPut = new TransformPutView();


        [HttpGet]
        public IHttpActionResult DetailedTypes()
        {
            List<DetailedTypeViewModel> typores = TrRepo.GetAllTypes();
            return Json<List<DetailedTypeViewModel>>(typores);
        }
        [HttpGet]
        public IHttpActionResult Types()
        {
            List<TypeViewModel> typores = TrRepo.GetTypesforEnum();
            return Json<List<TypeViewModel>>(typores);
        }
        [HttpGet]
        public IHttpActionResult Companies()
        {
            List<CompanyViewModel> companies = TrRepo.GetCompaniesforEnum();
            return Json<List<CompanyViewModel>>(companies);
        }
        [HttpGet]
        [Route("api/PrProduct/products/{Typo:alpha}")]
        public IHttpActionResult Products(string Typo)
        {
            List<PrProductViewModel> products = TrRepo.GetAllProducts(Typo);
            return Json<List<PrProductViewModel>>(products);
        }
        [HttpGet]
        [Route("api/PrProduct/productsbycat/{Cat:alpha}")]
        public IHttpActionResult ProductsByCategories(string Cat)
        {
            List<PrProductViewModel> products = TrRepo.GetAllProductsbyCat(Cat);
            return Json<List<PrProductViewModel>>(products);
        }
        [HttpGet]
        public IHttpActionResult AllProducts()
        {
            List<PrProductModelBriefView> products = TrRepo.GetAllProducts();
            return Json<List<PrProductModelBriefView>>(products);
        }
        [HttpGet]
        public IHttpActionResult Productdetail(int Id)
        {
            ProductDetailViewModel product = TrRepo.GetProductDetailsById(Id);
            return Json<ProductDetailViewModel>(product);
        }
        [HttpGet]
        public IHttpActionResult EventDetails(int id)
        {
            DetailedEventViewModel dEvent = TrRepo.DetailedEvent(id);
            return Json<DetailedEventViewModel>(dEvent);
        } 
        [HttpGet]
        public IHttpActionResult Product(int Id)
        {
            PrProductViewModel product = TrRepo.GetProductbyId(Id);
            return Json<PrProductViewModel>(product);
        }
        [HttpGet]
        public IHttpActionResult ProductsBrief()
        {
            List<PrProductModelBriefView> products = TrRepo.ProductBriefs();
            return Json<List<PrProductModelBriefView>>(products);
        }
        [HttpGet]
        public IHttpActionResult Categories()
        {
            List<CategoryViewModel> categories = TrRepo.GetAllCategories();
            return Json<List<CategoryViewModel>>(categories);
        }
        [HttpGet]
        public IHttpActionResult Category(int id)
        {
            CategoryViewModel category = TrRepo.GetCategorybyId(id);
            return Json<CategoryViewModel>(category);
        }
        [HttpGet]
        public IHttpActionResult Employees()
        {
            List <EmployeeViewModel> employees = TrRepo.GetEmployeesForEnumiration();
            return Json<List<EmployeeViewModel>>(employees);
        }
        public async Task <IHttpActionResult> AddProduct([FromBody] PostProductDetailModel Product)
        {
            await TrPost.AddProduct(Product);
            return Ok();
        }
        public IHttpActionResult AddEvent([FromBody] PostNewEventModel Event)
        {
            TrPost.AddEvent(Event);
            return Ok();
        }
        public IHttpActionResult AddNewEvent([FromBody] PostNewEventModel Event)
        {
            TrPost.AddNewEvent(Event);
            return Ok();
        }
        public async Task<IHttpActionResult> AddCompany([FromBody] PostCompanyModel Company)
        {
            await TrPost.AddCompany(Company);
            return Ok();
        }

        public async Task<IHttpActionResult> AddCategory([FromBody] CategoryViewModel Category)
        {
            await TrPost.AddCategory(Category);
            return Ok();
        }
        public async Task<IHttpActionResult> AddType([FromBody] PostTypeModel Type)
        {
            await TrPost.AddType(Type);
            return Ok();
        }
       
        public IHttpActionResult UpdateImage([FromBody] PutProductImage Image)
        {
            try
            {
                TrPut.UpdateImage(Image);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        public IHttpActionResult UpdateEventDate([FromBody] PostNewEventModel UpEvent)
        {
            try
            {
                TrPut.UpdateEventEndDate(UpEvent);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
     
        public IHttpActionResult ReturnProduct([FromBody] PostNewEventModel UpEvent)
        {
            try
            {
                TrPut.ReturnProduct(UpEvent);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        public async Task<HttpResponseMessage> UploadData()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {

                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 2024 * 2024 * 1; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 5 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {



                            var filePath = HttpContext.Current.Server.MapPath("~/Uploads/Images/" + postedFile.FileName);

                            postedFile.SaveAs(filePath);
                            var message1 = string.Format("Image Updated Successfully.");
                            return Request.CreateErrorResponse(HttpStatusCode.Created, filePath); ;

                        }
                    }

                   
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
        }
    }
}
