﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrStockModels.Models;
using PrStockModels.Repository;

namespace PrStock.Models
{
    public class PrProductViewModel
    {
        public int ProductViewModelId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public string Person { get; set; }
        public string EventName { get; set; }
        public int State { get; set; }
        public int EventTaken { get; set; }
        public int Quantity { get; set; }
        public string PictureUrl { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
    public class PrProductModelBriefView
    {
        public int PrProductModelBriefId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Quantity { get; set; }
        public int State { get; set; }
        public string Company { get; set; }
        public int? QuantityTaken { get; set; }
    }
    public class DetailedTypeViewModel
    {
        public int DetailedTypeViewModelId { get; set; }
        public string TypeName { get; set; }
        public string Category { get; set; }
        public int Quantity { get; set; }
    }
    public class TypeViewModel
    {
        public int TypeViewModelId { get; set; }
        public string TypeName { get; set; }
    }
    public class CategoryViewModel
    {
        public int CategoryViewModelId { get; set; }
        public string CategoryName { get; set; }
    }
    public class CompanyViewModel
    {
        public int CompanyViewModelId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyWebsite { get; set; }
    }
    public class CompanyContactsViewModel
    {
        public int CompanyContactsId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
    }
    public class ProductDetailViewModel
    {
        public int ProductDetailViewModelId { get; set; }
        public string ProductName { get; set; }
        public string Type { get; set; }
        public string Detail { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public int State { get; set; }
        public string PictureUrl { get; set; }
        public string TemplateUrl { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public CompanyContactsViewModel CompanyContacts { get; set; }
        public IEnumerable<EventViewModel> Events { get; set; }
    }
    public class PostProductDetailModel
    {
        public int ProductDetailViewModelId { get; set; }
        public string ProductName { get; set; }
        public string Detail { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public string PictureUrl { get; set; }
        public string TemplateUrl { get; set; }
        public int TypeId { get; set; }
        public int ProductState { get; set; }
        public int CompanyId { get; set; }
        public PostEventModel Event { get; set; } 
    }
    

    public class PostEventModel
    {
        public int PostEventViewModelId { get; set; }
        public string EventName { get; set; }
        public string EventDescription { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; } 
        public string EventUri { get; set; }
        public string EventType { get; set; }
        public int PrCategoryId { get; set; }
        public int PersonId { get; set; }
        public int Quantity { get; set; }
        public int ProductViewModelId { get; set; }
    }
    public class PostCompanyModel
    {
        public int PostCompanyModelId { get; set; }
        public string PostCompanyName { get; set; }
        public virtual CompanyContactModel CompanyContacts { get; set; }
        public string CompanyWebsite { get; set; }
    }

    public class EventViewModel
    {
        public int EventViewModelId { get; set; }
        public string EventName { get; set; }
        public DateTime StartDate{ get; set; }
        public DateTime? EndDate { get; set; }
        public int Quantity { get; set; }
        public string Category { get; set; }
        public string EventType { get; set; }
        public string RespPerson {get;set;}
    }
    public class DetailedEventViewModel
    {
        public int DetailedViewModelId { get; set; }
        public string EventName { get; set; }
        public string EventDescription { get; set; }
        public string EventUrl { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Category { get; set; }
        public string EventType { get; set; }
        public string RespPerson { get; set; }
        public IEnumerable<PrProductModelBriefView> Products { get; set; }
    }
    public class PostTypeModel
    {
        public string TypeName {get;set;}
    }
    public class EmployeeViewModel
    {
        public int EmployeeViewModelId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
    }
    public class PostNewEventModel
    {
        public int PostNewEventModelId { get; set; }
        public string EventName { get; set; }
        public string EventDescription { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EventUri { get; set; }
        public string EventType { get; set; }
        public int PersonId { get; set; }
        public int Quantity { get; set; }
        public List<EnumProduct> Products { get; set;}
    }
    public class EnumProduct
    {
        public int ProductId { get; set; }
        public int TakenQuantity { get; set; }
        public int CategoryId { get; set; }
    }
    public class PutProductImage
    {
        public int ProductId { get; set; }
        public string ImageUrl { get; set; }
    }
}