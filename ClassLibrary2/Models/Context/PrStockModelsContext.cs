﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace PrStockModels.Models
{
    public class PrStockModelsContext : DbContext
    {
        public DbSet<PrProductModels> Products { get; set; }
        public DbSet<PrCompanyModels> Companies { get; set; }
        public DbSet<PrEventModels> Events { get; set; }
        public DbSet<PrTypeModels> Types { get; set; }
        public DbSet<PrCategoryModels> Categories { get; set; }
        public DbSet<CompanyContactModel> Contact { get; set; }
        public DbSet<RespPersonModels> Employees { get; set; }
        public DbSet<PrProductModelsPrEventModels> PrPrProductEvents { get; set; }

        public PrStockModelsContext() : base("PrStockContext")
        {
            Database.SetInitializer<PrStockModelsContext>(new PrStockModelsInitializer());
        }
        public class PrStockModelsInitializer:DropCreateDatabaseIfModelChanges<PrStockModelsContext>
        {
            protected override void Seed(PrStockModelsContext context)
            {
                RespPersonModels Person1 = new RespPersonModels
                {
                    RespFirstName = "Anna",
                    RespLastName = "Koltakova",
                    ContactPhone = "+7 (909) 205-3788",
                    ContactEmail = "anna.kjltakova@dsr-corporation.com"
                };
                RespPersonModels Person2 = new RespPersonModels
                {
                    RespFirstName = "Sergej",
                    RespLastName = "Ryzhikov",
                    ContactPhone = "+7 (909) 205-3788",
                    ContactEmail = "sergey.ryzhikov@dsr-corporation.com"
                };
                PrTypeModels Type1  = new PrTypeModels
                {
                    Name = "Pens"
                };
                PrTypeModels Type2 = new PrTypeModels
                {
                    Name = "Cups"
                };
                PrTypeModels Type3 = new PrTypeModels
                {
                    Name = "Umbrella"
                };
                PrTypeModels Type4 = new PrTypeModels
                {
                    Name = "Tshirt"
                };
                PrCompanyModels Company = new PrCompanyModels
                {
                    CompanyName = "ООО ПапарикаПринт",
                    CompanyContacts = new CompanyContactModel
                    {
                        FirstName = "Наташа",
                        LastName = "Агупова",
                        Email = "paprika-print@yandex.ru",
                        Mobile = "8 (473) 295-47-10"
                    }
                };
                PrProductModels Product1 = new PrProductModels
                {
                    ProductName = "White Pen",
                    ProductDetails = "White Pen with Logo",
                    ProductImage = "http://voz.dsr-corporation.com:58281/Uploads/Images/WhitePlasticPen%20withLogo.jpg",
                    ProductPrice = 123.56,
                    ProductType = Type1,
                    ProductTemplate = "http://voz.dsr-corporation.com:58281/Uploads/Images/WhitePlasticPen%20withLogo.jpg",
                    Company = Company,
                    Stated =23
                };
                PrProductModels Product2 = new PrProductModels
                {
                    ProductName = "Silver",
                    ProductDetails = "White Cup with Logo",
                    ProductImage = "http://voz.dsr-corporation.com:58281/Uploads/Images/DSRCup.jpg",
                    ProductPrice = 320.56,
                    ProductType = Type2,
                    ProductTemplate = "http://voz.dsr-corporation.com:58281/Uploads/Images/DSRCup.jpg",
                    Company = Company,

                    Stated = 23
                };
                PrProductModels Product3 = new PrProductModels
                {
                    ProductName = "Black Umbrella",
                    ProductDetails = "Black Umbrella with Logo",
                    ProductImage = "http://voz.dsr-corporation.com:58281/Uploads/Images/DSRCup.jpg",
                    ProductPrice = 470.56,
                    ProductType = Type3,
                    ProductTemplate = "http://voz.dsr-corporation.com:58281/Uploads/Images/DSRCup.jpg",
                    Company = Company,
                    Stated = 5
                };
                PrProductModels Product4 = new PrProductModels
                {
                    ProductName = "White T-Shirt",
                    ProductDetails = "White Umbrella with Logo",
                    ProductImage = "http://voz.dsr-corporation.com:58281/Uploads/Images/T-Shirt1.jpg",
                    ProductPrice = 670.51,
                    ProductType = Type4,
                    ProductTemplate = "http://voz.dsr-corporation.com:58281/Uploads/Images/T-Shirt1.jpg",
                    Company = Company,
                    Stated = 5
                };
                PrCategoryModels Category1 = new PrCategoryModels
                {
                    Name = "HandOut"
                };
                PrCategoryModels Category2 = new PrCategoryModels
                {
                    Name = "VIP"
                };
                PrEventModels Event1 = new PrEventModels
                {
                    EventTitle = "RIF 2018",
                    EventDescription = "Вig IT event in Voronezh",
                    EventType = "Taken",
                    EventUrl = null,
                    EventStartDate = DateTime.Now,
                    EventEndDate = DateTime.Now,
                    RespPerson = Person1,
                };
                PrEventModels Event2 = new PrEventModels
                {
                    EventTitle = "Js BootCamp 2019",
                    EventDescription = "Вig IT event in Voronezh",
                    EventType = "Invoice",
                    EventUrl = null,
                    EventStartDate = DateTime.Now,
                    EventEndDate = DateTime.Now,
                    RespPerson = Person2,
                };
                PrProductModelsPrEventModels PrPrPrEvent1 = new PrProductModelsPrEventModels
                {
                    Product = Product1,
                    Event = Event1,
                    ProdQuantity = 420,
                    Quantity = 30,
                    Category = Category1
                    
                };
                PrProductModelsPrEventModels PrPrPrEvent2 = new PrProductModelsPrEventModels
                {
                    Product = Product2,
                    Event = Event1,
                    ProdQuantity = 420,
                    Quantity = 30,
                    Category = Category1
                };
                PrProductModelsPrEventModels PrPrPrEvent3 = new PrProductModelsPrEventModels
                {
                    Product = Product3,
                    Event = Event1,
                    ProdQuantity = 420,
                    Quantity = 30,
                    Category = Category2
                };
                PrProductModelsPrEventModels PrPrPrEvent4 = new PrProductModelsPrEventModels
                {
                    Product = Product1,
                    Event = Event2,
                    ProdQuantity = 420,
                    Quantity = 30,
                    Category = Category1
                };
                PrProductModelsPrEventModels PrPrPrEvent5 = new PrProductModelsPrEventModels
                {
                    Product = Product4,
                    Event = Event2,
                    ProdQuantity = 420,
                    Quantity = 30,
                    Category = Category2
                };
                context.Categories.Add(Category1);
                context.Categories.Add(Category2);
                context.Events.Add(Event1);
                context.Events.Add(Event2);
                context.Employees.Add(Person1);
                context.Employees.Add(Person2);
                context.PrPrProductEvents.Add(PrPrPrEvent1);
                context.PrPrProductEvents.Add(PrPrPrEvent2);
                context.PrPrProductEvents.Add(PrPrPrEvent3);
                context.PrPrProductEvents.Add(PrPrPrEvent4);
                context.PrPrProductEvents.Add(PrPrPrEvent5);
                context.SaveChanges();
            }
            
        }  
    }
}

