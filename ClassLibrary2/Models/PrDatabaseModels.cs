﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PrStockModels.Models
{
     public class PrProductModels
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int PrProductModelsId { get; set;}
        public string ProductName { get; set;}
        public virtual PrTypeModels ProductType { get; set;}
        public int Stated { get; set; }
        public string  ProductImage {get; set;}
        public string ProductTemplate {get;set;}
        public double ProductPrice { get; set; }
        public string ProductDetails { get; set; }
        public virtual PrCompanyModels Company { get; set; }
        [ForeignKey("ProductType")]
        public int PrTypeModelsId { get; set; }
        [ForeignKey("Company")]
        public int PrCompanyModelsId { get; set; }
        public virtual ICollection<PrProductModelsPrEventModels> PrProductsPrEvents { get; set; }
    }

    public class PrCompanyModels
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int PrCompanyModelsId { get; set; }
        public string CompanyName { get; set; }
        [ForeignKey("CompanyContacts")]
        public int ContactId { get; set; }
        public virtual CompanyContactModel CompanyContacts {get; set;}
        public string CompanyWebsite { get; set; }
        public virtual List <PrProductModels> PrProducts { get; set; }
    }

    public class CompanyContactModel
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int CompanyContactModelId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
      //  public virtual PrCompanyModels Company {get;set;}
    }

    public class PrEventModels
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int PrEventModelsId { get; set; }
        public string EventTitle { get; set; }
        public string EventDescription { get; set; }
        public DateTime EventStartDate { get; set; }
        public DateTime? EventEndDate { get; set; }
        public string EventUrl { get; set; }
        public string EventType { get; set; }    
        public virtual RespPersonModels RespPerson {get;set;}
        [ForeignKey("RespPerson")]
        public int RespPersonModelsId { get; set; }
        public virtual ICollection<PrProductModelsPrEventModels> PrProductsPrEvents { get; set; }
    }
    public class PrProductModelsPrEventModels
    {
        
        [Key, Column(Order = 0)]
        public int PrProductModelsId { get; set; }
        [Key, Column(Order = 1)]
        public int PrEventModelsId { get; set; }
        public virtual PrEventModels Event { get; set; }
        public virtual PrProductModels Product { get; set; }
        public int Quantity { get; set; }
        public int ProdQuantity { get; set; }
        [ForeignKey("Category")] 
        public int PrCategoryId { get; set; }
        public virtual PrCategoryModels Category { get; set; }
    }

    public class RespPersonModels
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int RespPersonModelsId { get; set; }
        public string RespFirstName { get; set; }
        public string RespLastName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public virtual List <PrEventModels> Events { get; set; }
    }

    public class PrCategoryModels
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int PrCategoryModelsId { get; set; }
        public string Name { get; set; }
        public virtual List<PrProductModelsPrEventModels> PrEv { get; set; }
    }

    public class PrTypeModels
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int PrTypeModelsId { get; set;}
        public string Name { get; set;}
        public virtual List <PrProductModels> Products { get; set;}
    }
   
}
