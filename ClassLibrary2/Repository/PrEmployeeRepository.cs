﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrStockModels.Models;

namespace PrStockModels.Repository
{
   public class PrEmployeeRepository
    {

            public List<RespPersonModels> GetAllEmployees()
            {
                using (var db = new PrStockModelsContext())
                {
                    return db.Employees.ToList();
                }

            }
            public RespPersonModels GetEmployeeById(int id)
            {
                using (var db = new PrStockModelsContext())
                {
                    return db.Employees.Where(x => x.RespPersonModelsId == id).FirstOrDefault();
                }
            }
            public async Task InsertAsync(RespPersonModels Employee)
            {
                using (var db = new PrStockModelsContext())
                {
                    db.Employees.Add(Employee);
                    await db.SaveChangesAsync();
                }
            }
            public async Task UpdateAsync(RespPersonModels Employee)
            {
                 using (var db = new PrStockModelsContext())
                 {
                     RespPersonModels employee = db.Employees.Where(x => x.RespPersonModelsId == Employee.RespPersonModelsId).FirstOrDefault();
                     employee = Employee;                     
                     db.Entry(employee).State=System.Data.Entity.EntityState.Modified;
                     await db.SaveChangesAsync();
                 }
        }
    }
}
