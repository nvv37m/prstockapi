﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrStockModels.Models;

namespace PrStockModels.Repository
{
    public class PrCompanyRepository
    {
        public List<PrCompanyModels> GetAllCompanies()
        {
            using (var db = new PrStockModelsContext())
            {
               return db.Companies.ToList();
            }
            
        }
        public PrCompanyModels GetCompanyById(int id)
        {
            using (var db = new PrStockModelsContext())
            {
                 return db.Companies.Where(x=>x.PrCompanyModelsId==id).FirstOrDefault();
            }
        }
        public async Task InsertAsync(PrCompanyModels Company)
        {
            using (var db = new PrStockModelsContext())
            {
                db.Companies.Add(Company);
                await db.SaveChangesAsync();
            }
        } 
    }
}
