﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrStockModels.Models;
using System.Data.Entity;

namespace PrStockModels.Repository
{
   public class PrProductRepository
    {
        public async Task DeleteAsync(int id)
        {
            using (var db = new PrStockModelsContext())
            {
                PrProductModels TypeDel = new PrProductModels { PrProductModelsId = id };
                db.Products.Attach(TypeDel);
                db.Entry(TypeDel).State = EntityState.Deleted;
                await db.SaveChangesAsync();
            }
        }
        public void UpdateImage(PrProductModels product)
        {
            using (var db = new PrStockModelsContext())
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void UpdateAsync(int id, int OutQuantity,int EQuantity, PrEventModels _event)
        {
            using (var db = new PrStockModelsContext())
            {
                PrProductModels product = db.Products
                        .Where(x => x.PrProductModelsId == id)
                        .Include(x => x.PrProductsPrEvents.Select(p => p.Event).Select(s => s.RespPerson))
                        .Include(x=>x.ProductType)
                        .Include(x => x.Company.CompanyContacts)
                        .FirstOrDefault();
                db.Entry(product).State = System.Data.Entity.EntityState.Modified;
                PrProductModelsPrEventModels prevprprod = new PrProductModelsPrEventModels
                {
                    Product =product,
                    Event = _event,
                    Quantity=EQuantity,
                    ProdQuantity = OutQuantity,                
                };
                db.PrPrProductEvents.Add(prevprprod);
                db.SaveChanges();
            }

        }
        public async Task InsertAsync(PrProductModels ProductIns, PrEventModels EventIns,int CategoryId, int QTaken, int QOut)
        {
            using (var db = new PrStockModelsContext())
            {
                PrProductModelsPrEventModels ProdAdd = new PrProductModelsPrEventModels
                {
                    Product = ProductIns,
                    Event = EventIns,
                    Quantity = QTaken,
                    ProdQuantity = QOut,
                    PrCategoryId = CategoryId
                };
                db.PrPrProductEvents.Add(ProdAdd);
                await db.SaveChangesAsync();
            }
        }
        public IEnumerable<PrProductModels> GetAllProductsBrief()
        {
            using (var db = new PrStockModelsContext())
            {
                return db.Products
                    .Include(x =>x.ProductType)
                    .Include(x=>x.PrProductsPrEvents)
                    .ToList();
            }
        }
        public List<PrProductModels> GetAllProducts()
        {
            using (var db = new PrStockModelsContext())
            {
                return db.Products
                    .Include(x=>x.ProductType)
                    .Include (x=>x.Company)
                    .Include(x=>x.PrProductsPrEvents)
                    .ToList();
            }
        }
        public List<PrProductModels> GetAllProductsBriefById(int id)
        {
            using (var db = new PrStockModelsContext())
            {
                List<PrProductModelsPrEventModels> Events= db.PrPrProductEvents.Where(x => x.PrEventModelsId == id).ToList();
                List<PrProductModels> Products = new List<PrProductModels>();
                foreach (PrProductModelsPrEventModels pr in Events)
                {
                    PrProductModels product = pr.Product;
                    Products.Add(product);
                }
                return Products;

            }
        }
        public IEnumerable<PrProductModels> GetAllProductsByCategory(string Cat)
        {
            using (var db = new PrStockModelsContext())
            {
                if (db.Products != null)
                {
                    return db.PrPrProductEvents.Where(x => x.Category.Name == Cat)
                        .Select(y => y.Product)
                        .Include(y=>y.ProductType)
                        .Include(x => x.PrProductsPrEvents.Select(p => p.Category))
                        .Include(y=>y.PrProductsPrEvents.Select(z=>z.Event.RespPerson))
                        .ToList();
                }
                else { return null; };
            }
        }
        public IEnumerable<PrProductModels> GetAllProducts(string type)
        {
            using (var db = new PrStockModelsContext())
            {
                if (db.Products != null) {
                    return db.Products
                        .Where(x => x.ProductType.Name == type)
                        .Include(x => x.PrProductsPrEvents.Select(p =>p.Event).Select(xs => xs.RespPerson))
                        .Include(x => x.PrProductsPrEvents.Select(p => p.Category))
                        .Include(x=>x.Company)
                        .Include(x=>x.ProductType)
                        .ToList(); }
                else { return null; }
            };
        }
        public PrProductModels GetProductbyId (int id)
        {
            using (var db = new PrStockModelsContext())
            {
                try
                {
                    return db.Products
                        .Where(x => x.PrProductModelsId == id)
                        .Include(x => x.PrProductsPrEvents.Select(p => p.Event).Select(px =>px.RespPerson))
                        .Include(x => x.PrProductsPrEvents.Select(p=>p.Category))
                        .Include(x => x.ProductType)
                        .Include(x => x.Company.CompanyContacts)
                        .FirstOrDefault();                      
                }
                catch
                {
                    return null;
                }
            }
                
        }
    }
}
