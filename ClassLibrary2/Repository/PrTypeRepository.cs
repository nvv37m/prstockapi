﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrStockModels.Models;
using System.Data.Entity;

namespace PrStockModels.Repository
{
    public class PrTypeRepository
    {
        public async Task DeleteAsync(int id)
        {
            using (var db = new PrStockModelsContext())
            {
                PrTypeModels TypeDel = new PrTypeModels { PrTypeModelsId = id };
                db.Types.Attach(TypeDel);
                db.Entry(TypeDel).State = EntityState.Deleted;
                await db.SaveChangesAsync();
            }
        }
        public async Task UpdateAsync(PrTypeModels TypeUpd)
        {
            using (var db = new PrStockModelsContext())
            {
                db.Types.Attach(TypeUpd);
                db.Entry(TypeUpd).State = System.Data.Entity.EntityState.Modified;
                await db.SaveChangesAsync();
            }
            
        }
        public async Task InsertAsync(PrTypeModels TypeIns)
        {
            using (var db = new PrStockModelsContext())
            {
                db.Types.Add(TypeIns);
                await  db.SaveChangesAsync();
            }
        }

        public IEnumerable<PrTypeModels> GetAllTypes()
        { 
            using (var db = new PrStockModelsContext())
            {
                if (db.Types != null) {
                    return db.Types
                        .Include(x => x.Products.Select(p=>p.PrProductsPrEvents.Select(px => px.Event)))
                        .Include(x =>x.Products.Select(p=>p.PrProductsPrEvents.Select(px=>px.Category)))
                        .ToList(); }
                else { return null; }
            }
        }
        public IEnumerable<PrTypeModels> GetTypesForEnum()
        {
            using (var db = new PrStockModelsContext())
            {
                return db.Types.ToList();
            }
        }
        public PrTypeModels GetTypebyId(int id)
        {
            using (var db = new PrStockModelsContext())
            {
                if (db.Types != null)
                {
                    return db.Types.Where(x => x.PrTypeModelsId == id).FirstOrDefault();
                }

                else
                {
                    PrTypeModels errorReq = new PrTypeModels() { Name = "Категория не найдена" };
                    return errorReq;
                }
            }
        
        }

       

    }
}
