﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrStockModels.Models;

namespace PrStockModels.Repository
{
    public class PrCategoryRepository
    {
   
        public List<PrCategoryModels> GetCategoriesforEnum()
        {
            using (var db = new PrStockModelsContext())
            {
                return db.Categories.ToList();
            }
        }
        public async Task InsertAsync(PrCategoryModels Category)
        {
            using (var db = new PrStockModelsContext())
            {
                db.Categories.Add(Category);
                await db.SaveChangesAsync();
            }
        }
        public PrCategoryModels GetCategorybyId(int id)
        {
            using (var db = new PrStockModelsContext())
            {
                    return db.Categories.Where(x => x.PrCategoryModelsId == id).FirstOrDefault();   
            }
        }
    }
}
