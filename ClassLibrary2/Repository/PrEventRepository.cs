﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrStockModels.Models;
using System.Data.Entity;

namespace PrStockModels.Repository
{
    public class PrEventRepository
    {
        public List<PrEventModels> GetAllEvents()
        {
            using (var db = new PrStockModelsContext())
            {
                if (db.Events != null)
                {
                    return db.Events
                        .Include(x =>x.PrProductsPrEvents)
                        .Include(x =>x.RespPerson)
                        .ToList();
                }
                else
                {
                    List<PrEventModels> NoPr = new List<PrEventModels>();
                    return NoPr;
                }
            }        
          }
        public PrEventModels GetEventById(int id)
        {
            using (var db = new PrStockModelsContext())
            {
                return db.Events
                    .Where(x => x.PrEventModelsId == id)
                    .Include(x => x.PrProductsPrEvents.Select(y => y.Product))
                    .Include(x => x.PrProductsPrEvents.Select(y => y.Product.ProductType))
                    .Include(x => x.PrProductsPrEvents.Select(y => y.Category))
                    .Include(x => x.RespPerson)
                    .FirstOrDefault();
            }
        }
        public PrProductModelsPrEventModels GetLastPrEventById(int id)
        {
            using (var db = new PrStockModelsContext())
            {   List<PrProductModelsPrEventModels> Chosen = db.PrPrProductEvents.Where(x => x.PrProductModelsId == id).ToList();

                return Chosen.Last();
            }
        }
           
        public void AddNewEvent(List<PrProductModelsPrEventModels>PrEvs )
        {
            using (var db = new PrStockModelsContext()) 
            {
                db.PrPrProductEvents.AddRange(PrEvs);
                db.SaveChanges();

            }
        }
        public void UpdateEvent(PrProductModelsPrEventModels PrEv)
        {
            using (var db = new PrStockModelsContext())
            {
                db.Entry(PrEv).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
        public void UpdateEventDate(PrEventModels Event)
        {
            using (var db = new PrStockModelsContext())
            {
                db.Entry(Event).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
        public PrProductModelsPrEventModels GetPrEvbyId(int PrId, int EvId)
        {
            PrProductModelsPrEventModels prEv = new PrProductModelsPrEventModels();
            using (var db = new PrStockModelsContext())
            {
               prEv = db.PrPrProductEvents.Where(x => x.PrEventModelsId == EvId)
                    .Where(y => y.PrProductModelsId == PrId).FirstOrDefault();
                return prEv;
            }
        }
            

            

        } 

}
